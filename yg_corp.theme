<?php

/**
 * @file
 * YG Corp theme implementations.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function yg_corp_form_system_theme_settings_alter(&$form, FormStateInterface &$form_state, $form_id = NULL) {
  $form['header_settings']['header'] = [
    '#type' => 'details',
    '#title' => t('Header Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['header_settings']['header']['header_lite'] = [
    '#type' => 'checkbox',
    '#title' => t("select white header"),
    '#default_value' => theme_get_setting('header_lite'),
    '#description' => t('Check the box to change menu header style'),
  ];
  $form['background_image'] = [
    '#type' => 'managed_file',
    '#title'    => t('banner image'),
    '#default_value' => theme_get_setting('background_image'),
    '#upload_location' => 'public://',
    '#description' => t('Choose your banner image for commom pages eg:- 404, 403'),
  ];
  // Work-around for a core bug affecting admin themes. See issue #943212.
  $form['footer_settings']['footer'] = [
    '#type' => 'details',
    '#title' => t('Footer Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['footer_settings']['footer']['copyright'] = [
    '#type' => 'text_format',
    '#title' => t('Copyrights'),
    '#default_value' => theme_get_setting('copyright')['value'],
    '#description'   => t("Please enter the copyright content here."),
  ];

  $form['footer_settings']['footer']['facebook'] = [
    '#type'          => 'textfield',
    '#title'         => t('facebook'),
    '#default_value' => theme_get_setting('facebook'),
    '#description'   => t("Give URL of the social media."),
  ];

  $form['footer_settings']['footer']['twitter'] = [
    '#type'          => 'textfield',
    '#title'         => t('twitter'),
    '#default_value' => theme_get_setting('twitter'),
    '#description'   => t("Give URL of the social media."),
  ];

  $form['footer_settings']['footer']['instagram'] = [
    '#type'          => 'textfield',
    '#title'         => t('instagram'),
    '#default_value' => theme_get_setting('instagram'),
    '#description'   => t("Give URL of the social media."),
  ];

  $form['footer_settings']['footer']['youtube'] = [
    '#type'          => 'textfield',
    '#title'         => t('youtube'),
    '#default_value' => theme_get_setting('youtube'),
    '#description'   => t("Give URL of the social media."),
  ];
  $form['footer_settings']['footer']['email'] = [
    '#type'          => 'textfield',
    '#title'         => t('email'),
    '#default_value' => theme_get_setting('email'),
    '#description'   => t("Give URL of the social media."),
  ];
  $form['footer_settings']['footer']['address_title'] = [
    '#type'          => 'textfield',
    '#title'         => t('address title'),
    '#default_value' => theme_get_setting('address_title'),
    '#description'   => t("Give URL of the social media."),
  ];
  $form['footer_settings']['footer']['address'] = [
    '#type'          => 'textfield',
    '#title'         => t('address'),
    '#default_value' => theme_get_setting('address'),
    '#description'   => t("Give URL of the social media."),
  ];
  $form['footer_settings']['footer']['contact'] = [
    '#type'          => 'textfield',
    '#title'         => t('contact'),
    '#default_value' => theme_get_setting('contact'),
    '#description'   => t("Give URL of the social media."),
  ];
}

/**
 * Implements hook_preprocess_node().
 */
function yg_corp_preprocess_node(&$variables) {
  // Comment Count.
  $nid = $variables['node']->nid->value;
  $database = \Drupal::database();
  $num_comment = $database->query('SELECT comment_count FROM {comment_entity_statistics} WHERE entity_id = ' . $nid)->fetchAssoc();
  if ($num_comment != NULL) {
    $variables['comment_count'] = $num_comment['comment_count'];
  }
}

/**
 * Implements hook_preprocess_page().
 */
function yg_corp_preprocess_page(&$variables) {
  $variables['basepath'] = $GLOBALS['base_url'];
  $variables['copyright'] = theme_get_setting('copyright')['value'];
  $variables['facebook'] = theme_get_setting('facebook');
  $variables['twitter'] = theme_get_setting('twitter');
  $variables['instagram'] = theme_get_setting('instagram');
  $variables['youtube'] = theme_get_setting('youtube');
  $variables['email'] = theme_get_setting('email');
  $variables['address_title'] = theme_get_setting('address_title');
  $variables['address'] = theme_get_setting('address');
  $variables['contact'] = theme_get_setting('contact');
  $variables['header_lite'] = theme_get_setting('header_lite');
  // background_image
  // $background_image = array();
  // $path = array();
  // $fid = theme_get_setting('background_image' , 'yg_corp')[0];
  // for ($i = 1; $i <= $fid; $i++) {
  // if (!empty($fid)) {
  //     $file = \Drupal\file\Entity\File::load($fid);
  //     $path = file_create_url($file->getFileUri());
  //   }
  // }
  // $variables['background_image'] = $path;.
}

/**
 * Implements hook_library_info_alter().
 */
function yg_corp_library_info_alter(&$libraries, $ext) {
  unset($libraries['form']);
}

/**
 * Implements hook_form_submit().
 */
function yg_corp_form_submit(&$form, $form_state) {
  $fid = $form_state->getValue('background_image');
  if (isset($fid[0])) {
    $file = File::load($fid[0]);
    if (!empty($file)) {
      $file->setPermanent();
      $file->save();
      $file_usage = \Drupal::service('file.usage');
      $file_usage->add($file, 'yg_corp', 'themes', 1);
    }
  }
}

/**
 * Implements hook_preprocess_views_view_unformatted().
 */
function yg_corp_preprocess_views_view_unformatted(&$variables) {
  $variables['is_front'] = Drupal::service('path.matcher')->isFrontPage();
}

/**
 * Implements hook_preprocess_menu().
 */
function yg_corp_preprocess_menu(&$variables, $hook) {
  // We're doing that for main menu.
  if ($hook == 'main') {
    // Get the current path.
    $current_path = \Drupal::request()->getRequestUri();
    $items = $variables['items'];
    foreach ($items as $key => $item) {
      // If path is current_path, set active to li.
      if ($item['url']->toString() == $current_path) {
        // Add active link.
        $variables['items'][$key]['attributes']['class'] = 'active';
      }
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function yg_corp_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  // Get Request Object.
  $request = \Drupal::request();
  // If there is HTTP Exception..
  if ($exception = $request->attributes->get('exception')) {
    // Get the status code.
    $status_code = $exception->getStatusCode();
    if (in_array($status_code, [403, 404])) {
      $suggestions[] = 'page__' . $status_code;
    }
  }
}

/**
 * Implements hook_theme().
 */
function yg_corp_theme(&$existing, $type, $theme, $path) {
  $hooks = [];
  $hooks['user_login_form'] = [
    'render element' => 'form',
    'template' => 'user-login-form',
  ];
  return $hooks;
}
